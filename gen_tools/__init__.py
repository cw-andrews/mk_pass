"""
Functions and accompanying CLI Tools to generate pseudo-random passwords and
UUIDs (GUIDs)
"""

from .gen_pass_cli import gen_pass
from .gen_uuid_cli import gen_uuid

__version__ = "0.1.2"
